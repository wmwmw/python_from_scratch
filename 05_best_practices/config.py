VERSION = None
APP_NAME = "mrkev"
SAVE_FILE = "kedluben.txt"

FULL_NAME = f"{APP_NAME} {VERSION}"

if not (VERSION and APP_NAME and SAVE_FILE):
    raise ValueError("Missing config parameters")