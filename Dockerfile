FROM jupyterhub/singleuser:latest
ENV JUPYTERHUB_SINGLEUSER_APP='jupyter_server.serverapp.ServerApp'
COPY ./01_python_basics /home/jovyan/basics
COPY ./02_python_technology /home/jovyan/technology
COPY ./03_testing /home/jovyan/testing
COPY ./04_python_advanced /home/jovyan/advanced
COPY ./05_best_practices /home/jovyan/best_practices
COPY ./06_sql /home/jovyan/sql
COPY ./07_db_programming /home/jovyan/db_programming