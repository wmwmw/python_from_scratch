from client import ApiClient

class CzTranslator:
    def __init__(self):
        self.client = ApiClient("matous", "mrkev")

    def translate_from_en(self, word):
        response = self.client.translate(word, 'cz')  # {"word": "dog", "translated_word": "pes"}
        return response["translated_word"]