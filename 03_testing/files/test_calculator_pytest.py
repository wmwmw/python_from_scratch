import pytest

from calculator import Calculator


def test_init_calc():
    calculator = Calculator(100)
    assert calculator.current_value == 100
    assert calculator.results_history == []


@pytest.mark.parametrize("initial_value,added_value,expected_result", [(1, x, x + 1) for x in range(10000)])
def test_add(initial_value, added_value, expected_result):
    calculator = Calculator(initial_value)
    calculator.add(added_value)
    assert calculator.current_value == expected_result
    assert calculator.results_history == [initial_value]


def test_subtract():
    calculator = Calculator(11)
    calculator.subtract(-2)
    assert calculator.current_value == 13
    assert calculator.results_history == [11]
