import requests


class ApiClient:
    def __init__(self, username, password):
        self.username = username
        self.password = password

    def translate(self, word, language):
        result = requests.post("https://prekladac.cz/api",
                              json={"word": word, "language": language},
                              auth=(self.username, self.password))
        
        return result.json()