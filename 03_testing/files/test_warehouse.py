import pytest

from warehouse import Warehouse, Item


@pytest.fixture
def empty_warehouse():
    return Warehouse()

@pytest.fixture
def carrot():
    return Item(1, "Carrot")

@pytest.fixture
def tomato():
    return Item(2, "Tomato")


@pytest.fixture
def warehouse_with_items(empty_warehouse, carrot, tomato):
    empty_warehouse.stock(carrot)
    empty_warehouse.stock(tomato)
    return empty_warehouse


def test_stock_item(empty_warehouse, carrot):
    result = empty_warehouse.stock(carrot)
    assert result
    assert 1 == len(empty_warehouse._items)


def test_unstock_tomato(warehouse_with_items, tomato):
    item = warehouse_with_items.unstock_id(tomato.id)
    assert tomato == item
    assert 1 == len(warehouse_with_items._items)

