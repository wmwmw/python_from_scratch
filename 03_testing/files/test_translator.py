import unittest
from unittest import mock

from translator import CzTranslator


class TranslatorTestCase(unittest.TestCase):

    def setUp(self):
        self.translator = CzTranslator()

    @mock.patch("client.ApiClient.translate")
    def test_translate_word(self, mock_method):
        mock_method.return_value = {"word": "carrot", "translated_word": "mrkev"}
        self.assertEqual("mrkev", self.translator.translate_from_en("carrot"))
