CREATE TABLE student (
    id BIGSERIAL PRIMARY KEY,
    first_name VARCHAR NOT NULL,
    surname VARCHAR NOT NULL,
    age INT NOT NULL,
    home_address TEXT
);