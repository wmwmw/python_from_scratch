CREATE TABLE customer (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    surname VARCHAR NOT NULL,
    first_name VARCHAR NOT NULL
);

CREATE TABLE airport (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    icao_code VARCHAR(4) NOT NULL,
    full_name VARCHAR NOT NULL
);

CREATE TABLE flight (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    origin BIGINT REFERENCES airport(id),
    destination BIGINT REFERENCES airport(id),
    capacity INT NOT NULL
);

CREATE TABLE flight_ticket (
    customer_id BIGINT REFERENCES customer(id),
    flight_id BIGINT REFERENCES flight(id),
    used BOOL NOT NULL DEFAULT false,
    PRIMARY KEY (account_id, role_id)
)