import calculator


x = int(input("Prvni: "))
y = int(input("Druhe: "))


operation = input("Operace [add, power]: ")


if func := getattr(calculator, operation, None):
    result = func(x, y)
    print(f"Vysledek: {result}")

else:
    print("Neplatna operace")